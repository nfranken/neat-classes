Gem::Specification.new do |s|
	s.name			= 'neat-classes'
	s.version		= '0.0.1'
	s.date			= '2013-05-13'
	s.summary		= %q{Foundation style classes for span-columns using Bourbon Neat}
	s.description	= %q{Foundation style classes for span-columns using Bourbon Neat}
	s.authors		= ["Nick Franken"]
	s.email			= ["nf@the42.com"]
	s.files			= ["lib/neat-classes.rb", "lib/neat-classes/engine.rb", "app/assets/stylesheets/neat-breakpoint-classes.scss", "app/assets/stylesheets/breakpoints/large.scss", "app/assets/stylesheets/breakpoints/medium.scss", "app/assets/stylesheets/breakpoints/small.scss"]
	s.add_dependency 'neat'
end