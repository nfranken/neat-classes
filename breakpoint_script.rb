# generates breakpoint files
breakpoints = %w( large medium small )

breakpoints.each do |breakpoint|
	File.open("app/assets/stylesheets/breakpoints/#{breakpoint}.scss", "w") do |f|
		f << "@include media($#{breakpoint}) {\n"
			1.upto(12).each do |num|
				f << "\t.#{breakpoint}-#{num}{\n"
					f << "\t\t@include span-columns(#{num});\n"
					unless num == 1
						1.upto(num).each do |inside_num|
							f << "\t\t.#{breakpoint}-#{inside_num}-of-#{num}{\n"
								f << "\t\t\t@include span-columns(#{inside_num} of #{num});\n"
							f << "\t\t}\n"
						end
					end
				f << "\t}\n"
			end
			1.upto(12).each do |num|
				f << "\t.#{breakpoint}-push-#{num}{\n"
					f << "\t\t@include shift(#{num});\n"
				f << "\t}\n"
				f << "\t.#{breakpoint}-pull-#{num}{\n"
					f << "\t\t@include shift(-#{num});\n"
				f << "\t}\n"
			end
		f << "}"

	end
end
