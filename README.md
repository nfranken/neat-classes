# Classes for [Bourbon Neat](http://neat.bourbon.io/) similar to [Foundation Framework grid classes](http://foundation.zurb.com/docs/components/grid.html)



# Installation
---	
* add `gem "neat-classes"` to your Gemfile after `gem "neat"`
* bundle install
* in `application.css` in between `@import "bourbon"` and `@import "neat"` include small medium and large media query variables:

```
@import "bourbon";
$small: new-breakpoint(min-width 320px max-width 480px);
$medium: new-breakpoint(min-width 481px max-width 1024px);
$large: new-breakpoint(min-width 1025px);
@import "neat";
```

* after that `@import "neat-breakpoint-classes";`


# Usage
---
### [Grid](http://neat.bourbon.io/docs/#span-columns)
You can use classes like `.large-12, .medium-6 .small-4`.  Div would be 12 columns on a large display, 6 on a medium and 4 in a small media query.

### [Nesting](http://neat.bourbon.io/docs/#span-columns)
Nested div's can by handled by using `.large-6-of-12` style classes.

### [Push / Pull](http://neat.bourbon.io/docs/#shift)
Pushing or Pulling can be achieved with `.medium-pull-6` or `.small-push-3`.